Convertir documents HTML a XHTML
================================

MP4UF1A3T3

Tècniques de correcció i adaptació de documents HTML

La sopa d’etiquetes de l’HTML
-----------------------------

-   El navegadors sempre han acceptat HTML amb defectes, encara que no
    està definit com es mostrarà aquest.
-   A l’inici de la Web no existien eines de validació.
-   Molts autors de pàgines Web no tenen formació apropiada.
-   Fins i tot les eines [WYSIWYG](http://ca.wikipedia.org/wiki/WYSIWYG)
    poden generar HTML incorrecte.
-   Pot ser necessari migrar codi HTML, encara que correcte, a XHTML.
-   Els documents vàlids poden no ser correctes segons altres criteris,
    com ara la seva accesibilitat.

L’eina `tidy`(1) és perfecte per tractar tot tipus de problemes en HTML
antic o incorrecte.

Tipus MIME
----------

-   Els [tipus
    MIME](http://es.wikipedia.org/wiki/Multipurpose_Internet_Mail_Extensions)
    especifiquen el tipus de contingut dels fitxers.
-   Poden ser definits pel servidor web, o obtinguts a partir de
    l’extensió del fitxer.
-   La correspondència entre extensions i tipus pot ser establerta pel
    gestor d’escriptori, de forma centralitzada (`/etc/mime.types`) o
    pel navegador.
-   Sempre farem servir el tipus `text/html` (extensió `.html`) per
    documents HTML i XHTML.
-   El tipus MIME `application/xhtml+xml` (extensió `.xhtml`) no està
    plenament suportat pels navegadors.

Enllaços recomanats
-------------------

-   [HTML Tidy Project](http://tidy.sourceforge.net/)
-   [Dave Raggett’s Overview](http://www.w3.org/People/Raggett/tidy/)
    ([local](aux/TIDY/tidy.html))
-   [Wikipedia: HTML Tag Soup](http://en.wikipedia.org/wiki/Tag_soup)

Pràctiques
----------

-   Instal·la si cal el paquet tidy. Estudia els fitxers instal·lats amb
    el paquet, documentació, etc.
-   Segueix, íntegrament, el tutorial de Dave Ragget citat més amunt.
-   Què significa, en el text mostrat en fer `tidy -help | less`, *Use
    --optionX valueX for any configuration option "optionX" with
    argument "valueX". For a list of the configuration options, use
    "-help-config" or refer to the man page.* .
-   Prepara un script del shell que, cridant a `tidy`, converteixi
    documents HTML a XHTML estricte. Opcions que et poden fer servei:
    -   `clean: yes`
    -   `doctype: transitional`
    -   `doctype: strict`
    -   `drop-font-tags: yes`
    -   `drop-proprietary-attributes: yes`
    -   `logical-emphasis: yes`
    -   `numeric-entities: yes`
    -   `output-xhtml: yes`
    -   `indent: auto`
    -   `tidy-mark: no`
    -   `alt-text: Text…`
-   Processa aquesta [pàgina](http://sheldonbrown.com/web_sample1.html)
    amb Tidy, convertida a XHTML *transicional*, fins que no es generin
    missatges d’error o avís.

