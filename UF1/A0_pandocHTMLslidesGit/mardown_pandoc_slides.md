
# Per què els desenvolupadors i sysadmins hem d'utilitzar presentacions HTML en lloc de paquets ofimàtics?

La tendència actual quan es produeix documentació científica és fer-la en markdown, el més natural és utilitzar el markdown per produir una presentació en `fitxer pla`, que no sigui "esclava" de cap programari.

# A qui va adreçada aquesta presentació?

A tots els professor@s del departament d'Informàtica, però en especial al
professorat de segon dels nostres cicles formatius, els quals hauran de vetllar perquè
els alumnes utilitzin:

* markdown
* git
* pandoc

# Què necessito per crear i visualitzar una presentació HTML?

* editor de text (qualsevol)
* pandoc instal·lat
* navegador d'Internet (qualsevol)

# markdown, pandoc & presentacions HTML

[Pandoc](https://pandoc.org/) és una llibreria _Haskell_ usada per convertir fitxers d'un format a d'altre.

S'executa com una ordre en consola amb una sèrie d'opcions.

# Què es pot fer amb pandoc?

- Conversió entre formats (html, markdown, docx, odt, PDF ...)  
- Producció de presentacions (creació de Diapositives amb `slidy` i `beamer` (classe LaTeX)


# Paquets a instal·lar

* [pandoc](https://pandoc.org/ "Pàgina oficial de Pandoc")
* aspell, aspell-ca, aspell-es, ...
* texlive

```
dnf -y install pandoc aspell texlive
```
```
pandoc --help
```

# Exemples d'ús amb Pandoc


```
pandoc -s --toc -c pandoc.css -A footer.html MANUAL.txt -o example3.html

pandoc -s  aux/header2.txt mardown_pandoc_slides.md  -t slidy -o pandoc.html

pandoc -s  aux/header.yml mardown_pandoc_slides.md  -t slidy -o pandoc.html

```
[Demos](https://pandoc.org/demos.html)

## Repassar ortografia

Abans de passar el pandoc, passem el aspell pel fitxer origen markdown:
```
    aspell --lang=ca check README.md
```


# Crear presentacions en format HTML

* Usarem un format de passi de presentacions [slidy](http://www.w3.org/Talks/Tools/Slidy) o [revealjs](http://lab.hakim.se/reveal-js/) entre d'[altres](https://pandoc.org/).

* A partir d'un fitxer en format markdown, li passarem el pandoc

# Exemples per crear una presentació HTML:
```
pandoc --self-contained  prueba.md -t slidy -o output.html

pandoc -s --toc mardown_pandoc_slides.md  aux/header.yml -t slidy -o pandoc.html
```
# Opcions bàsiques:

* -f (from) (en casos molt especials, habitualment no s'usa)
* -t (to)
* -o (output)
* -s,--standalone, versió lleugera, fa crida als estils.
* --self-contained: versió pesada, descarrega tot el necessari al codi HTML
* --toc
* fitxer.yml
* -c styles.css
* -i  
    els ítems de les llistes apareixeran de manera incremental

# Fer passi de diapositives

Carregar el fitxer HTML amb un navegador

# Dreceres:

* C: mostrar la toc
* S: Fer petita la font
* B: Augmenta la font  

![](aux/logo.png)

# Exemples de fitxers de metadades
* Bloc de títol:
```
% Markdown, Pandoc i presentacions HTML
% Jordi Andúgar
% 27 de juny 2019
```

Format YAML:
```
---
title: Markdown, Pandoc i presentacions HTML
author: Jordi Andúgar
...
```

# Vull afegir vistositat a la meva presentació

CSS!
```
pandoc -s -c aux/style.css aux/header2.txt mardown_pandoc_slides.md -t slidy -o pandoc.html
```

# Diapositives amb format PDF

Usarem el format de passi de presentacions `beamer`

* Amb fitxer de metadades (titol, autor, tema, ...)
```
pandoc -t beamer mardown_pandoc_slides.md aux/beamer.yml -o pandoc.pdf
```

* O podem utilitzar variables de metadades en la consola:
```
pandoc -t beamer mardown_pandoc_slides.md -V author:Jordi -o pandoc.pdf
```

-V per passar-li variables de metadades o bé usar un fitxer amb metadades

El temes es poden trobar [aquí](http://deic.uab.es/~iblanes/beamer_gallery/) ó
[aquí](https://hartwork.org/beamer-theme-matrix/).


`Opcions` per a pandoc `beamer`

* --metadata-file fitxer (text, [YAML](https://en.wikipedia.org/wiki/YAML "human-readable data-serialization language"), etc)


# Exemples de conversions en general

|Destí|Origen|Ordre|Observacions|
|-----|------|-----|:------------:|
|PDF|md|pandoc fitxer.md -o fitxer.pdf||
|md|html|pandoc test.html -o test.md --parse-raw||
|md|docx|pandoc -s example30.docx -t markdown -o example35.md||
|pdf|md|pandoc -s 09_DDL.md -t beamer -o 09_DDL.pdf|`pdf de diapositives`|
|html|md|pandoc -t slidy --self-contained 09_DDL.md -o prova.html|`html de diapositives`|
|html|md|pandoc -t slidy --standalone 09_DDL.md -o prova.html|`html de diapositives`|

# Pràctiques

Des de consola, toca provar tot el que s'ha explicat:

- _firefox/chromium_ per executar la presentació HTML\
- _evince_ per la presentació pdf\
- Esborreu els fitxers HTML i PDF
- Torneu a crear-los amb pandoc i les diferentes opcions


#
![](aux/cc.png "Licència Creative Commons")
